package Bob;

sub trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s }

sub silence  { my $s = shift; return trim($s) eq '' }
sub shouting { my $s = shift; return $s eq uc($s) && $s ne lc($s) }
sub question { my $s = shift; return $s =~ /\?$/ }

sub hey {
	my $input = shift;
	return 'Fine. Be that way!' if (silence($input));
	return 'Woah, chill out!'   if (shouting($input));
	return 'Sure.'              if (question($input));
	return 'Whatever.'
}

1;