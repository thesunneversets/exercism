package Phrase;

use strict;
use warnings;

sub word_count {
    my $phrase = shift;
    my @words = split(/\W+/, $phrase);
    my $href = {};
    foreach my $word (@words) {
        $href->{ lc($word) }++;
    }
    return $href;
}

1;