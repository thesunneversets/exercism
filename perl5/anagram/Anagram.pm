package Anagram;

use strict;
use warnings;

sub normalize {
	my $word = shift;
	return (join '', sort split (//, lc($word)));
}

sub match {
    my ($word, @candidates) = @_;
    my $sorted_word = normalize($word);
    my @matches;
    foreach my $candidate (@candidates) {
  	    if (normalize($candidate) eq $sorted_word) {
	        push (@matches, $candidate) unless $candidate eq $word;
  	    }
    }
    return \@matches;
}

1;