package DNA;

sub to_rna {
    my $dna = shift;
    $dna =~ tr/ACGT/ACGU/;
    return $dna;
}

1;