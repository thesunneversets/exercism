(function () {
    'use strict';

    function School(name) {
        this.name = name;
        this.db = [];
    }

    School.prototype.add = function (pupil, grade) {
        this.db[grade] = this.db[grade] || [];
        this.db[grade].push(pupil);
    };

    School.prototype.grade = function (grade) {
        return this.db[grade] || [];
    };

    School.prototype.sort = function () {
        for (var prop in this.db) {
            if (this.db.hasOwnProperty(prop)) {
                this.db[prop].sort();
            }
        }
        return this.db;
    };

    module.exports = School;
})();