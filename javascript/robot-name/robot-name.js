var reservedRobotNames = reservedRobotNames || [];

var Robot = function() {

    var generate_name = function() {

        var name = "";
        var type = "";
        var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var numbers = "0123456789";

        for (var i = 0; i < 5; i++) {
            type = i < 2 ? letters : numbers;
            name += type.charAt(Math.floor(Math.random() * type.length));
        }

        return name;

    }

    this.reset = function() {

        var reservedRobotsIndex = reservedRobotNames.indexOf(this.name);
        if (reservedRobotsIndex > -1) {
            reservedRobotNames.splice(reservedRobotsIndex, 1);
        }

        this.name = generate_name();
        while (this.name in reservedRobotNames) {
            this.name = generate_name();
        }
        reservedRobotNames.push(this.name);
    }

    this.reset();

}
module.exports = Robot;