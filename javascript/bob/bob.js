// method defined on the prototype, as per Henrik's suggestion -
// if anyone can help explain the benefits of such an approach I'd appreciate it!

function Bob(){}

Bob.prototype.hey = function(input) {
	var silence  = input === '',
		shouting = input === input.toUpperCase(),
		question = input.slice(-1) === '?';

		return silence?  "Fine. Be that way!" :
		   	   shouting? "Woah, chill out!" :
		       question? "Sure." :
		                 "Whatever.";
}

module.exports = Bob;