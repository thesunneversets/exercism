function DNA(string) {
	this.string = string;
}

DNA.prototype = {

    get nucleotideCounts () { 
    	return this.string.split('').reduce(function(counts, nucleotide){
        	counts[nucleotide]++;
        	return counts;
    	}, { 'A':0, 'T':0, 'C':0, 'G':0 });
	}

}

DNA.prototype.count = function(nucleotide){

    if ("ACGTU".indexOf(nucleotide) === -1) {
        throw new Error("Invalid Nucleotide");
    }
    return this.nucleotideCounts[nucleotide] || 0;

}

module.exports = DNA;