(function () {
    'use strict';

    function DNA(strand) {
        this.strand = strand;
    }

    DNA.prototype.hammingDistance = function (strand2) {
        return this.strand
                .substr(0, strand2.length)
                .split('')
                .reduce(function (prev, curr, indx) {
                    return curr === strand2.charAt(indx) ? prev : prev + 1;
                }, 0);
    };

    module.exports = DNA;

}());