var Year = function(year) {
	this.year = year;
}

Year.prototype.isDivisibleBy = function(n) {
	return this.year % n === 0;
}

Year.prototype.isLeapYear = function() {
	return this.isDivisibleBy(4)
		&& ( ! this.isDivisibleBy(100) || this.isDivisibleBy(400) );
}

var isLeapYear = function(year) {
	var year = new Year(year);
    return year.isLeapYear();
};

module.exports = isLeapYear;