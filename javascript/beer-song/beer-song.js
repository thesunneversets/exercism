function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function Beer(){

	var getVerseComponents = function (number) {
		switch (number) {
			case 2:
				return { bottles: "2 bottles", next: "1 bottle", action: "Take one down and pass it around" }
			case 1:
				return { bottles: "1 bottle", next: "no more bottles", action: "Take it down and pass it around" }
			case 0:
			    return { bottles: "no more bottles", next: "99 bottles", action: "Go to the store and buy some more" }
			default:
				return { bottles: number + " bottles", next: number-1 + " bottles", action: "Take one down and pass it around" }
		}
	}

    this.verse = function(number) { 
		var vc = getVerseComponents(number);
		
		return [capitalize(vc.bottles), " of beer on the wall, ",
		        vc.bottles, " of beer.\n",
                vc.action, ", ",
                vc.next, " of beer on the wall.\n"
			   ].join('');
     }

     this.sing = function (number, stopAt) {
        var stopAt = stopAt || 0,
            song = [];

        while (number >= stopAt) {
            song.push(this.verse(number--));
        }
        return song.join("\n");
     }

}

Beer.call(Beer);

module.exports = Beer;