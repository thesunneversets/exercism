function tokenize(phrase) {
	return phrase.match(/\w+/g);
}

function normalize(phrase) {
	return phrase.toLowerCase();
}

function Words(phrase) {
	this.phrase = phrase;
}

Words.prototype = {
	
	get count() {
		words = tokenize(normalize(this.phrase));
		return words.reduce(function(freqs, word){
			freqs[word] = freqs[word] || 0;
			freqs[word]++;
			return freqs;
		}, {}); 
	}
}
	
module.exports = Words;