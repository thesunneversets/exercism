function sortWord(word) {
	return word.split('').sort().join('');
}

function areAnagrams(word, sortedWord) {
	return sortWord(word) === sortedWord;
}

function Anagram(word) {
	var sortedWord = sortWord(word);
	this.isAnagramOf = function(word) { return areAnagrams(word, sortedWord); }
}

Anagram.prototype.match = function(wordlist) {
	return wordlist.filter(this.isAnagramOf);
}

module.exports = Anagram;