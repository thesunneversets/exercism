(function () {
    'use strict';

    function Phone(input) {

        var digits = input.replace(/[\D]/g, '');

        if (digits.length === 11 && digits.charAt(0) === '1') {
            digits = digits.substr(1);
        }

        if (digits.length !== 10) {
            digits = '0000000000';
        }

        this.digits = digits;

    }

    Phone.prototype.number = function () {
        return this.digits;
    };

    Phone.prototype.areaCode = function () {
       return this.digits.substr(0, 3);
    };

    Phone.prototype.lineNumber = function () {
        return this.digits.substr(3, 3) + "-" + this.digits.substr(6);
    };

    Phone.prototype.toString = function () {
        return "(" + this.areaCode() + ") " + this.lineNumber();
    }

    module.exports = Phone;
})();