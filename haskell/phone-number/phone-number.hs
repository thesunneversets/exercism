module Phone (number, areaCode, prettyPrint) where

import Data.Char (isNumber)
import Data.List.Split (splitPlaces)

number :: String -> String
number n
	| digits == 11 && head cleaned == '1' = tail cleaned
	| digits == 10 = cleaned
	| otherwise = "0000000000" 
	where cleaned = filter isNumber n
	      digits = length cleaned

areaCode :: String -> String
areaCode n = take 3 $ number n

prettyPrint :: String -> String
prettyPrint n = concat ["(", areaCode, ") ", exchange, "-", subscriberNo]
	where [areaCode, exchange, subscriberNo] = splitPlaces [3, 3, 4] $ number n