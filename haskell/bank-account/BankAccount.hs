module BankAccount ( BankAccount
                   , openAccount, closeAccount
                   , getBalance, incrementBalance) where
import Control.Concurrent.STM
import Control.Applicative

type BankAccount = TVar (Maybe Int)

openAccount :: IO BankAccount
openAccount = newTVarIO (Just 0)

closeAccount :: BankAccount -> IO ()
closeAccount acct = atomically $ writeTVar acct Nothing

getBalance :: BankAccount -> IO (Maybe Int)
getBalance = readTVarIO 

incrementBalance :: BankAccount -> Int -> IO (Maybe Int)
incrementBalance acct amt = atomically $ do
    bal <- fmap (+amt) <$> readTVar acct 
    writeTVar acct bal
    return bal