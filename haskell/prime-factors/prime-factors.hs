module PrimeFactors (primeFactors) where

isFactorOf :: Int -> Int -> Bool
isFactorOf n x = 
	n `mod` x == 0

primeFactors :: Int -> [Int]
primeFactors 1 = []
primeFactors n =
	let factor = head $ filter (isFactorOf n) [2..n]
	in [factor] ++ primeFactors (div n factor)

	