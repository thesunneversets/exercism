module DNA (count, nucleotideCounts) where

import Data.List (union)
import Data.Map (Map, fromListWith)

dna_nucleotides = "ACGT";
rna_nucleotides = "ACGU";
valid_nucleotides = union dna_nucleotides rna_nucleotides;

count :: Char -> String -> Int
count nucleotide strand
	| nucleotide `elem` valid_nucleotides = length $ filter (==nucleotide) strand
	| otherwise = error $ "invalid nucleotide " ++ show nucleotide

nucleotideCounts :: String -> Map Char Int
nucleotideCounts strand = fromListWith (+) 
						  $ zip strand (repeat 1) 
						  ++ zip dna_nucleotides (repeat 0)