module ETL (transform) where

import Data.Map (Map, fromList, toList)
import Data.Char (toLower)

lowerAll :: [String] -> [String]
lowerAll = map $ map toLower

transformElem :: (Int, [String]) -> [(String, Int)]
transformElem (score, letters) = [(letter, score) | letter <- lowerAll letters]

transform :: Map Int [String] -> Map String Int
transform input =  fromList . concatMap transformElem $ toList input