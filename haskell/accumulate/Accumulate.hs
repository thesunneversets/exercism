module Accumulate (accumulate) where

accumulate :: (t -> a) -> [t] -> [a]
accumulate f [] = []
accumulate f (x:xs) = f x : accumulate f xs