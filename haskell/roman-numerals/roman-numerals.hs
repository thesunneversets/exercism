module Roman (numerals) where

numerals :: Int -> String
numerals n = translate n values
  where values = [(1000, "M"),(900, "CM"), (500, "D"), (400, "CD"), 
                  (100, "C"), (90, "XC"), (50, "L"), (40, "XL"), 
                  (10, "X"), (9, "IX"), (5, "V"), (4, "IV"), (1, "I")]

translate :: Int -> [(Int, String)] -> String
translate n values
  | null values = ""
  | arabicVal > n = translate n remainingValues
  | otherwise = romanVal ++ translate (n - arabicVal) values
  where (arabicVal, romanVal) : remainingValues = values