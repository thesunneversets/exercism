module Luhn 
    ( checkDigit
    , addends
    , checksum
    , isValid
    , create
    ) where

checkDigit :: Integer -> Integer
checkDigit n = n `mod` 10

toDigitList :: Integer -> [Integer]
toDigitList 0 = []
toDigitList n = n `mod` 10 : toDigitList (n `div` 10)

addends :: Integer -> [Integer]
addends = reverse . zipWith ($) (cycle [id, alter]) . toDigitList

alter :: Integer -> Integer
alter n
  | result > 10 = result - 9
  | otherwise   = result
  where result = 2 * n

checksum :: Integer -> Integer
checksum n = (sum $ addends n) `mod` 10

isValid :: Integer -> Bool
isValid n = checkDigit (sum $ addends n) == 0

create :: Integer -> Integer
create n
  | cs == 0 = n'
  | otherwise = n' + 10 - cs
  where n' = n * 10
        cs = checksum n'