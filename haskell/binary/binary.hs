module Binary (toDecimal) where

toDecimal :: String -> Int
toDecimal b
  | all (`elem` "10") b = sum $ zipWith (\x y -> if x == '1' then 2^y else 0) b positions
  | otherwise = 0
  where len = length b
        positions = [len-1, len-2 .. 0]