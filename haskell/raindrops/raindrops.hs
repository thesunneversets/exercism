module Raindrops (convert) where

pling :: Int -> String
plang :: Int -> String
plong :: Int -> String
pling n = if n `mod` 3 == 0 then "Pling" else ""
plang n = if n `mod` 5 == 0 then "Plang" else ""
plong n = if n `mod` 7 == 0 then "Plong" else ""

convert :: Int -> String
convert n
	| null rainMusic = show n
	| otherwise = rainMusic
	where rainMusic = pling n ++ plang n ++ plong n 