module Anagram (anagramsFor) where

import Data.Char (toLower)
import Data.List (sort)

lc :: String -> String
lc = map toLower

isAnagramOf :: String -> String -> Bool
isAnagramOf x y = (sort . lc) x == (sort . lc) y

anagramsFor :: String -> [String] -> [String]
anagramsFor word wordlist =
	filter (isAnagramOf word) wordlist 