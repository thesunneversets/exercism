module CryptoSquare 
    (
      normalizePlaintext
    , squareSize
    , plaintextSegments
    , ciphertext
    , normalizeCiphertext
    ) where

import Data.Char (isAlphaNum, toLower)
import Data.List (transpose)
import Data.List.Split (chunksOf)

normalizePlaintext :: String -> String
normalizePlaintext = map toLower . filter isAlphaNum

squareSize :: String -> Int
squareSize  = ceiling . sqrt . fromIntegral . length

plaintextSegments :: String -> [String]
plaintextSegments text = chunksOf size text'
  where text' = normalizePlaintext text
        size = squareSize text'

ciphertext :: String -> String
ciphertext = concat . transpose . plaintextSegments

normalizeCiphertext :: String -> String
normalizeCiphertext = unwords . chunksOf 5 . ciphertext