module Queens (boardString, canAttack) where

import Data.List (intersperse)
import Data.List.Split (chunksOf)
import Data.Maybe (fromMaybe)

type Square = (Int, Int)

boardString :: Maybe Square -> Maybe Square -> String
boardString whiteQueen blackQueen = unlines . map (intersperse ' ') . chunksOf 8 $ map (depictSquare wq bq) allSquares
  where allSquares = [(i,j) | i <- [0..7], j <- [0..7]]
        wq = fromMaybe (8,8) whiteQueen
        bq = fromMaybe (8,8) blackQueen

depictSquare :: Square -> Square -> Square -> Char
depictSquare wq bq sq
  | wq == sq = 'W'
  | bq == sq = 'B'
  | otherwise = 'O'

canAttack :: Square -> Square -> Bool
canAttack sq1 sq2
  | x1 == y1 = True
  | x2 == y2 = True
  | x1-y1 == x2-y2 = True
  | otherwise = False
  where (x1, x2) = sq1
        (y1, y2) = sq2