module Meetup where

import Data.Time.Calendar (Day, fromGregorian, gregorianMonthLength)
import Data.Time.Calendar.WeekDate (toWeekDate)
import Data.Time.Format (formatTime)
import System.Locale (defaultTimeLocale)

data Weekday  = Sunday | Monday | Tuesday | Wednesday | Thursday | Friday | Saturday deriving (Enum)

data Schedule = Teenth | First | Second | Third | Fourth | Last

firstInMonth :: Weekday -> Integer -> Int -> Int
firstInMonth weekday year month
  | weekdayNo < monthStart = 8 + weekdayNo - monthStart
  | otherwise              = 1 + weekdayNo - monthStart
  where weekdayNo          = fromEnum weekday
        (_, _, monthStart) = toWeekDate $ fromGregorian year month 1

meetupDay :: Schedule -> Weekday -> Integer -> Int -> Day
meetupDay sched weekday year month = fromGregorian year month day
  where first = firstInMonth weekday year month
        daysInMonth = gregorianMonthLength year month
        day = case sched of
          Teenth -> if (first + 7 > 12) then first + 7 else first + 14
          First  -> first
          Second -> first + 7
          Third  -> first + 14
          Fourth -> first + 21
          Last   -> if (first + 28 > daysInMonth) then first + 21 else first + 28 