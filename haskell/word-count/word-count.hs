module WordCount (wordCount) where

	import Data.Char (toLower, isPunctuation, isSymbol)
	import Data.List (group, sort)
	import Data.Map (fromListWith)
	
	validChars :: String -> String
	validChars = filter (not . isPunctuation) . filter (not . isSymbol)
	
	lowerCase :: String -> String
	lowerCase = map toLower
	
	wordCount phrase = fromListWith (+) 
				$ map (\x -> (lowerCase x, 1))
				$ words 
				$ validChars phrase