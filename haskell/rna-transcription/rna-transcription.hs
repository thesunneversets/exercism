module DNA (toRNA) where 

    nucleotideSwap :: Char -> Char
    nucleotideSwap 'T' = 'U'
    nucleotideSwap  n  =  n

    toRNA :: String -> String
    toRNA = map nucleotideSwap