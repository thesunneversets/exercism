module Hexadecimal (hexToInt) where

import Data.Char (ord)
import Data.List (foldl')
import Data.Maybe (fromMaybe)

hexCharToInt :: Char -> Maybe Int
hexCharToInt n
  | enum > 47 && enum < 58 = Just (enum - 48)
  | enum > 96 && enum < 103 = Just (enum - 87) 
  | otherwise = Nothing
  where enum = ord n

hexToInt :: String -> Int
hexToInt = fromMaybe 0 . foldl' hexToInt' (Just 0)
  where hexToInt' x y = case hexCharToInt y of 
    						 Just n -> fmap ((+n).(*16)) x
    						 _ -> Nothing