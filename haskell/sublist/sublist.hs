module Sublist 
	( Sublist(..)
	, sublist
	) where

import Data.List (isInfixOf)

data Sublist = Equal | Sublist | Superlist | Unequal deriving (Show, Eq)

sublist :: Eq a => [a] -> [a] -> Sublist
sublist l1 l2
	| l1 == l2 = Equal
	| l2 `isInfixOf` l1 = Superlist
	| l1 `isInfixOf` l2 = Sublist
	| otherwise = Unequal