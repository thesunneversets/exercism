module SumOfMultiples 
	( sumOfMultiples
	, sumOfMultiplesDefault
	) where

sumOfMultiples :: Integral a => [a] -> a -> a
sumOfMultiples ms n = sum [x | x <- [0..n-1], x `isMultipleOfAnyOf` ms ]

sumOfMultiplesDefault :: Integer -> Integer
sumOfMultiplesDefault = sumOfMultiples [3, 5]

isMultipleOfAnyOf :: Integral a => a -> [a] -> Bool
isMultipleOfAnyOf n = any (n `isMultipleOf`)

isMultipleOf :: Integral a => a -> a -> Bool
isMultipleOf n m = n `mod` m == 0