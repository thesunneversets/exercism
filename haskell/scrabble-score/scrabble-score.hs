module Scrabble (scoreLetter, scoreWord) where

import Data.Array (listArray, (!))
import Data.Char (toUpper)

scoreLetter :: Char -> Int
scoreLetter letter = arr ! toUpper letter
  where arr = listArray ('A','Z') [1,3,3,2,1,4,2,4,1,8,5,1,3,1,1,3,10,1,1,1,1,4,4,8,4,10]

scoreWord :: [Char] -> Int
scoreWord = sum . map scoreLetter