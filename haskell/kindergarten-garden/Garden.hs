module Garden 
	(
	  garden
	, defaultGarden
	, lookupPlants
	, Plant(..)
	) where

import Data.Map (Map, fromList, findWithDefault) 
import Data.List (transpose, sort)
import Data.List.Split (chunksOf)

data Plant = Grass | Clover | Radishes | Violets deriving (Show, Eq)

charToPlant :: Char -> Plant
charToPlant 'G' = Grass
charToPlant 'C' = Clover
charToPlant 'R' = Radishes
charToPlant 'V' = Violets

stringToPlants :: String -> [Plant]
stringToPlants = map charToPlant

garden :: [String] -> String -> Map String [Plant]
garden children diagram = fromList $ zip children' cups
  where children' = sort children
        cups = map (stringToPlants . concat) 
               . transpose . map (chunksOf 2) 
               . lines $ diagram
        

defaultGarden :: String -> Map String [Plant]
defaultGarden = garden ["Alice", "Bob", "Charlie", "David", 
					    "Eve", "Fred", "Ginny", "Harriet", 
					    "Ileana", "Joseph", "Kincaid", "Larry"]

lookupPlants :: String -> Map String [Plant] -> [Plant]
lookupPlants = findWithDefault []