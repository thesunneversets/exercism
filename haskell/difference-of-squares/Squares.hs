module Squares (sumOfSquares, squareOfSums, difference) where

sumOfSquares :: Integral a => a -> a
sumOfSquares n = (2*n^3 + 3*n^2 + n) `div` 6

squareOfSums :: Integral a => a -> a
squareOfSums n = (n^4 + 2*n^3 + n^2) `div` 4

difference :: Integral a => a -> a
difference n = (3*n^4 + 2*n^3 - 3*n^2 - 2*n) `div` 12