module Bob where

	import Data.Char

	isSilence  s = all isSpace s
	isShouting s = not $ any isLower s
	isQuestion s = last s == '?'

	responseFor conversation
		| isSilence  conversation = "Fine. Be that way."
		| isShouting conversation = "Woah, chill out!"
		| isQuestion conversation = "Sure."
		| otherwise               = "Whatever."