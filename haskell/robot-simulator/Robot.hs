module Robot (
      Bearing(..)
    , Robot
    , mkRobot
    , coordinates
    , simulate
    , bearing
    , turnRight
    , turnLeft
    ) where

import Data.List (foldl')

data Bearing = North | West | South | East deriving (Show, Eq, Enum)
type Instruction = Char
type Coord = Int
data Robot = Robot { bearing :: Bearing
                   , coordinates :: (Coord, Coord) 
                   } deriving (Show, Eq) 

mkRobot :: Bearing -> (Coord, Coord) -> Robot
mkRobot = Robot

simulate :: Robot -> [Instruction] -> Robot
simulate = foldl' performInstruction
               
performInstruction :: Robot -> Instruction -> Robot                        
performInstruction robot instruction = case instruction of 
    'L' -> robot {bearing = turnLeft b}
    'R' -> robot {bearing = turnRight b}
    'A' -> case b of 
      North -> robot {coordinates = (x, y+1)}
      South -> robot {coordinates = (x, y-1)}
      East  -> robot {coordinates = (x+1, y)}
      West  -> robot {coordinates = (x-1, y)}
    where Robot b (x, y) = robot

turnRight :: Bearing -> Bearing
turnRight North = East
turnRight bearing = pred bearing

turnLeft :: Bearing -> Bearing
turnLeft East = North
turnLeft bearing = succ bearing