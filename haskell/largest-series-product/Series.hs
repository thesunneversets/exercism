module Series (digits, slices, largestProduct) where

import Data.Char (digitToInt)
import Data.List (tails)

digits :: String -> [Int]
digits = map digitToInt

slices :: Int -> String -> [[Int]]
slices n s = map digits $ consecutives n s

consecutives :: Int -> String -> [String]
consecutives n s = take (length s - n + 1) $ map (take n) (tails s)

largestProduct :: Int -> String -> Int
largestProduct n s =
  case slices n s of
    [] -> 1
    _  -> maximum $ map (foldr (*) 1) $ slices n s