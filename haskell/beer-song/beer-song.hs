module Beer (sing, verse) where

import Data.Char (toUpper)

capitalize :: String -> String
capitalize (h:t) = (toUpper h):t
capitalize [] = []

verseComponents :: Int -> (String, String, String)
verseComponents 2 = ("2 bottles", "1 bottle", "Take one down and pass it around")
verseComponents 1 = ("1 bottle", "no more bottles", "Take it down and pass it around")
verseComponents 0 = ("no more bottles", "99 bottles", "Go to the store and buy some more")
verseComponents n = (show n ++ " bottles", show (n - 1) ++ " bottles", "Take one down and pass it around")

sing :: Int -> Int -> String
sing startFrom stopAt = concat $ map ((++"\n") . verse) [startFrom, pred startFrom .. stopAt]

verse :: Int -> String
verse n = let (bottles, next, action) = verseComponents n
          in concat [capitalize bottles, " of beer on the wall, ",
 					 bottles, " of beer.\n", action, ", ",
		  			 next, " of beer on the wall.\n"]

          