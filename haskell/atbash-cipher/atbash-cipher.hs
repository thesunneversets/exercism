module Atbash (encode) where

import Data.Char (toLower, isAlphaNum, isAsciiLower)
import Data.List (intercalate)
import Data.List.Split (chunksOf)

encodeChar :: Char -> Char
encodeChar c
  | isAsciiLower c = toEnum (fromEnum 'a' + fromEnum 'z' - fromEnum c)
  | otherwise = c 

encode :: String -> String
encode = intercalate " " . chunksOf 5 . map (encodeChar . toLower) . filter isAlphaNum