module DNA (hammingDistance) where

hammingDistance strand1 strand2 = length $ filter not $ zipWith (==) strand1 strand2