module Strain
	( keep
	, discard
	) where

keep f [] = []
keep f (x:xs)
	| f x  = x : keep f xs
	| otherwise = keep f xs 

discard f = keep (not . f)