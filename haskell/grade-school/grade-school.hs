module School (School, School.empty, add, grade, sorted) where

import Prelude hiding (map)
import Data.Map (Map, empty, insertWith, findWithDefault, toAscList, map)
import Data.List (sort)

type School = Map Int [String]

empty :: School
empty = Data.Map.empty

add :: Int -> String -> School -> School
add g pupil = insertWith (++) g [pupil]

grade :: Int -> School -> [String]
grade g db = findWithDefault [] g db

sorted :: School -> [(Int, [String])]
sorted = toAscList . map sort