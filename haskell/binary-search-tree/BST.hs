module BST 
  ( bstLeft
  , bstRight
  , bstValue
  , singleton
  , insert
  , fromList
  , toList
  ) where

import Control.Applicative
import Data.Foldable
import Data.Monoid (mappend)

data Node a = Node {
  bstLeft :: Maybe (Node a),
  bstValue :: a,
  bstRight ::  Maybe (Node a) } deriving (Show)

instance Foldable Node where
  foldMap f (Node l x r) = ff l `mappend` f x `mappend` ff r
    where ff = foldMap (foldMap f)

singleton :: a -> Node a
singleton x = Node Nothing x Nothing

insert :: Ord a => a -> Node a -> Node a
insert n node@(Node l x r)
    | n > x = node { bstRight = maybeInsert n r }
    | otherwise = node { bstLeft = maybeInsert n l }

fromList :: Ord a => [a] -> Node a
fromList (x:xs) = foldl' (flip insert) (singleton x) xs

maybeInsert :: Ord a => a -> Maybe (Node a) -> Maybe (Node a)
maybeInsert x node = fmap (insert x) node <|> Just (singleton x)