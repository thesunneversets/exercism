module Robot (robotName, mkRobot, resetName) where

import Control.Concurrent.MVar (MVar, newMVar, readMVar, swapMVar)
import Control.Monad (void, replicateM, liftM2)
import Control.Applicative ((<*>), (<$>))
import System.Random (randomRIO)

randomRobot :: IO [Char]
-- randomRobot = liftM2 (++) (replicateM 2 $ randomRIO('A','Z')) (replicateM 3 $ randomRIO('0','9'))
randomRobot = (++) <$> (replicateM 2 $ randomRIO('A','Z')) <*> (replicateM 3 $ randomRIO('0','9'))

robotName :: MVar [Char] -> IO [Char]
robotName = readMVar

mkRobot :: IO (MVar [Char])
mkRobot = randomRobot >>= newMVar

resetName :: MVar [Char] -> IO ()
resetName robot = void $ randomRobot >>= swapMVar robot