(ns dna
	(:require [clojure.string :only [replace]]))

(defn to-rna
	"transcribe dna to rna sequence"
	[nucleotide-sequence]
	(let [thymidine "T" uracil "U"]
	(clojure.string/replace nucleotide-sequence thymidine uracil)))