(ns dna)

(defn hamming-distance [strand1 strand2]
	(count (filter false? (map = strand1 strand2))))