(ns dna)

(def ^:const valid-nucleotides "ACGTU")

(defn nucleotide-counts [string]
	(merge {\A 0 \C 0 \G 0 \T 0} (frequencies string)))

(defn count [nucleotide string]
	(if (some #{nucleotide} valid-nucleotides)
		(clojure.core/count (re-seq (re-pattern (str nucleotide)) string))
		(throw (Exception. "invalid nucleotide"))))

;;; previous iteration

(defn nucleotide-counts-old [string]
    (reduce
        #(update-in %1 [%2] inc)
        {\A 0 \C 0 \G 0 \T 0} 
        string))        

(defn count-old [nucleotide string]
    (if (some #{nucleotide} valid-nucleotides)
        (let [n-count ((nucleotide-counts-old string) nucleotide)]
            (if (nil? n-count) 0 n-count))
        (throw (Exception. "invalid nucleotide"))))

;;; nucleotide-count_test.clj changed under my feet!
;;; the following works with nucleotides as strings not chars.

(defn nucleotide-counts-strings [string]
	(reduce 
		#(update-in %1 [(str %2)] inc) 
		{"A" 0 "C" 0 "G" 0 "T" 0} 
		string))

(defn count-strings [nucleotide string]
	(if (some #{nucleotide} (map #(str %) valid-nucleotides))
		(let [n-count ((nucleotide-counts-strings string) nucleotide)]
			(if (nil? n-count) 0 n-count))
		(throw (Exception. "invalid nucleotide"))))