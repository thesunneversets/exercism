(ns bob
	(:use clojure.string))
	
(defn silence? [x]
	(blank? x))
	
(defn shouting? [x]
	(= x (upper-case x)))
	
(defn question? [x]
	(= (last x) \?))
	
(defn response-for 
	"surly teenager conversation simulator"
	[input]
	(cond (silence? input) "Fine. Be that way." 
		(shouting? input) "Woah, chill out!"
	    (question? input) "Sure."
	    :else "Whatever."))