(ns phrase
	(:require [clojure.string :as str]))

(defn- normalize [string]
	(str/lower-case string))

(defn- tokenize [string]
	(str/split string #"\W+"))

(defn word-count 
	"given a phrase, count the occurrences of each word"
	[phrase] 
	(-> phrase normalize tokenize frequencies))