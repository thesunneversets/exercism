(ns anagram)

(defn anagrams-for
  "given a word and a list of possible anagrams, select the correct one(s)"
  [word possible-anags]
  (let [target (frequencies word)]
  (filter #(= (frequencies %) target) possible-anags)))