(defn leap-year? [y] 
  (cond
    (= (mod y 400) 0) true
    (= (mod y 100) 0) false
    (= (mod y   4) 0) true 
	:else             false))