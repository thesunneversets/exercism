(declare random-robot-name)

(defn robot [] (atom (random-robot-name)))

(defn robot-name [robot] @robot)

(defn reset-name [robot] 
    (reset! robot (random-robot-name)))

(def ^:private from-zero (int \0))
(def ^:private to-nine (int \9))
(def ^:private NUMBERS (map char (range from-zero to-nine)))

(def ^:private from-A (int \A))
(def ^:private to-Z (int \Z))
(def ^:private UC (map char (range from-A to-Z)))

(defn- random [collection]
    (nth collection (rand (count collection))))

(defn- random-char [] (random UC))
(defn- random-number [] (random NUMBERS))

(defn- random-robot-name []
		(apply str
			(concat (take 2 (repeatedly random-char)) 
        	 		(take 3 (repeatedly random-number)))))