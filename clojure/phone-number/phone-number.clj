(ns phone)

(defn- digits-only [n]
	(apply str (re-seq #"\d+" n)))

(defn strip-leading-one? [n]
	(cond (not= (count n) 11) n
		  (not= (first n) \1) n
		  :else (subs n 1)))

(defn number [telno]
	(let [digits (-> telno digits-only strip-leading-one?)]
		(cond (not= (count digits) 10) "0000000000"
		:else digits
	)))

(defn area-code [telno]
	(subs (number telno) 0 3))
	
(defn- line-number [telno]
	(let [cleaned (number telno)]
	(format "%s-%s" (subs cleaned 3 6) (subs cleaned 6))))

(defn pretty-print [telno]
    (format "(%s) %s" (area-code telno) (line-number telno)))