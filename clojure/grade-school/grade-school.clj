(ns school)

(def db {})

(defn add [db pupil grade]
	(assoc-in db [grade] (concat (db grade) [pupil])))
	
(defn grade [db grade]
	(db grade []))

(defn sorted [db]
	(into {} (for [[k v] db] [k (sort v)])))