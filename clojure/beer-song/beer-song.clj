(ns beer
	(:require [clojure.string :as s :only [capitalize join]]))

(defn- verse-components [n]
	(cond 
		(= n 2) (hash-map :b "2 bottles" :n "1 bottle" :a "Take one down and pass it around")
		(= n 1) (hash-map :b "1 bottle" :n "no more bottles" :a "Take it down and pass it around")
		(= n 0) (hash-map :b "no more bottles" :n "99 bottles" :a "Go to the store and buy some more")
		:else (hash-map :b (str n " bottles") :n (str (dec n) " bottles") :a "Take one down and pass it around")))

(defn verse [number]
	(let [{bottles :b next :n action :a} (verse-components number)]
	(str (s/capitalize bottles) " of beer on the wall, " bottles " of beer.\n"
		  action ", " next " of beer on the wall.\n")))

(defn sing
 	([number] (sing number 0))
	([number, stopAt] 
	(s/join
		(map #(str % "\n")
			(map #(verse %) (range number (dec stopAt) -1))))))