open Core.Std 

let is_silence input = String.for_all ~f:Char.is_whitespace input
let is_shouting input = input <> String.lowercase input && input = String.uppercase input 
let is_question input = String.is_suffix input ~suffix:"?"

let response_for input =
  if is_silence input then "Fine. Be that way!"
  else if is_shouting input then "Woah, chill out!"
  else if is_question input then "Sure."
  else "Whatever."