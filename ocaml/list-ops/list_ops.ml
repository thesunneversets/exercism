open Core.Std

let length n = 
  let rec go acc = function
    | [] -> acc
    | _ :: t -> go (acc + 1) t in
  go 0 n

let rec fold ~init:acc ~f:(f) n = match n with
    [] -> acc
  | h::t -> fold ~init:(f acc h) ~f:(f) t 

let reverse n = 
  let rec go acc = function
    | [] -> acc
    | h::t -> go (h :: acc) t in
  go [] n

let filter ~f:(f) n = 
  let rec go ~f:(f) acc = function
    | [] -> acc
    | h::t -> if f h then go (h::acc) ~f:(f) t else go acc ~f:(f) t in
  go [] ~f:(f) (reverse n)

let append n1 n2 = 
  let rec go n2 = function
    | [] -> n2
    | h::t -> go (h::n2) t in
  go n2 (reverse n1)

let map ~f:(f) n =
  let rec go ~f:(f) acc = function
    | [] -> acc
    | h::t -> go ~f:(f) (f h :: acc) t in
  go [] ~f:(f) (reverse n)

let concat ns = 
  let rec go acc = function
    | [] -> acc
    | h::t -> go (append h acc) t in
  go [] (reverse ns)