hamming = {}

function hamming.compute(s1, s2)
  result = 0
  length = math.min(string.len(s1), string.len(s2))
  for i = 1, length do
  	if string.sub(s1, i, i) ~= string.sub(s2, i, i) then
  	  result = result + 1
  	end 
  end
  return result
end

return hamming