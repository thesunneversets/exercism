//
//  Leap.swift
//  Leap
//
//  Created by Matthew Marcus on 30/07/2014.
//  Copyright (c) 2014 Beggars. All rights reserved.
//

import Foundation

class Year {
    var year: Int
    init(calendarYear: Int) {
        year = calendarYear
    }
    var isLeapYear: Bool {
        switch year {
            case year where year%400 == 0:
                return true
            case year where year%100 == 0:
                return false
            case year where year%4 == 0:
                return true
            default:
                return false
            }
    }
}