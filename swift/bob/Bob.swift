import Foundation

class Bob {
    class func hey(input: String) -> String {
        var response: String
        switch (is_silence(input), is_shouting(input), is_question(input)) {
            case (true, _, _): response = "Fine, be that way."
            case (_, true, _): response = "Woah, chill out!"
            case (_, _, true): response = "Sure."
            case _:            response = "Whatever"
        }
        return response
    }
}

func is_silence(input: String) -> Bool {
    return input.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).isEmpty
}

func is_shouting(input: String) -> Bool {
    return input == input.uppercaseString && input != input.lowercaseString
}

func is_question(input: String) -> Bool {
    return input.hasSuffix("?")
}