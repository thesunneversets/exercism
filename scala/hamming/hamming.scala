object Hamming {
  def compute (strand1: String, strand2: String) = 
    strand1.zip(strand2).count{ case (a, b) => a != b }
}