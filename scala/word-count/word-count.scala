class Phrase(val p: String) {
  val NonwordCharacters = "[^a-z0-9']+"
  lazy val wordCount = {
    p.toLowerCase.split(NonwordCharacters).groupBy(identity).mapValues(_.size)
  }
}