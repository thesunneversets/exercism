class Bob() {
  def hey(input:String) = {
    if      ( isShouting(input) ) "Woah, chill out!"
    else if ( isQuestion(input) ) "Sure."
    else if ( isSilence (input) ) "Fine. Be that way!"
    else "Whatever."
  }

  private def isShouting(input:String) = input == input.toUpperCase && input != input.toLowerCase
  private def isQuestion(input:String) = input.endsWith("?")
  private def isSilence(input:String) = input.trim.isEmpty

}