<?php

require_once('sudoku.php');

class SudokuTest extends PHPUnit_Framework_TestCase {

	public function testInit()
	{
		$sudoku = new Sudoku;
		
		$this->assertEquals(count($sudoku->squares), 81);
		$this->assertEquals(count($sudoku->unitlist), 27);
		
		foreach ($sudoku->squares as $s) {
			$this->assertEquals(count($sudoku->units[$s]), 3);
			$this->assertEquals(count($sudoku->peers[$s]), 20);
		}
		
		$this->assertEquals($sudoku->units['C2'], [
			['A2', 'B2', 'C2', 'D2', 'E2', 'F2', 'G2', 'H2', 'I2'],
            ['C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9'],
            ['A1', 'A2', 'A3', 'B1', 'B2', 'B3', 'C1', 'C2', 'C3']
        ]);
		$this->assertEquals($sudoku->peers['C2'], 
			['A2', 'B2', 'D2', 'E2', 'F2', 'G2', 'H2', 'I2',
             'C1', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9',
             'A1', 'A3', 'B1', 'B3']
        );
	}

	public function testUnconstrainedGrid()
	{
		$sudoku = new Sudoku;

		$values = $sudoku->unconstrained_grid();

		$this->assertEquals(count($values['A1']), 9);

		$this->assertEquals($sudoku->get_other_values($values['A1'], 0), $values['A1']);
		$this->assertEquals(count($sudoku->get_other_values($values['A1'], 1)), 8);
	}

	public function testAssign()
	{
		$sudoku = new Sudoku;

		$values = $sudoku->unconstrained_grid();

		$this->assertNotFalse($sudoku->assign($values, 'A1', 0));
	}

	public function testSolveSampleGrid()
	{
		$sudoku = new Sudoku;

		$grid1 = '003020600900305001001806400008102900700000008006708200002609500800203009005010300';

		//$this->assertEquals($sudoku->parse_grid($grid1)['A1'][0], 4);
		$this->assertEquals($sudoku->parse_grid($grid1)['A2'][0], 8);
		$this->assertEquals($sudoku->parse_grid($grid1)['A3'][0], 3);

		$this->assertTrue( $sudoku->display($sudoku->parse_grid($grid1)) );

	}

	public function testEliminate()
	{
		$sudoku = new Sudoku;

		$values = ['A3'=>[1,2,3]];
		$this->assertEquals($sudoku->eliminate($values, 'A3', 4), $values);

		$singleValue = ['A3'=>[1]];
		$this->assertFalse($sudoku->eliminate($singleValue, 'A3', 1));

		$values = $sudoku->unconstrained_grid();
		$this->assertEquals($sudoku->eliminate($values, 'A3', 0), $values);
		$this->assertEquals($sudoku->eliminate($values, 'A3', 3), $values);

		$grid = $sudoku->grid_values('003020600900305001001806400008102900700000008006708200002609500800203009005010300');
		$this->assertEquals($grid['A1'], 0);
		$this->assertEquals($grid['A3'], 3);
		$this->assertEquals($grid['B1'], 9);

	}

}