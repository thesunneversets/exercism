<?php

class Sudoku {

	public function __construct($grid) {

		$this->grid     = $grid;

		$this->digits   = str_split('123456789');
		$this->rows     = str_split('ABCDEFGHI');
		$this->cols     = $this->digits;
		
		$this->squares  = $this->cross($this->rows, $this->cols);
		
		foreach ($this->cols as $c) {
			$unitlist[] = $this->cross($this->rows, [$c]);
		}
		foreach ($this->rows as $r) {
			$unitlist[] = $this->cross([$r], $this->cols);
		}
		foreach (array_chunk($this->rows, 3) as $rs) {
			foreach (array_chunk($this->cols, 3) as $cs) {
				$unitlist[] = $this->cross($rs, $cs);
			}
		}
		$this->unitlist = $unitlist;

		foreach ($this->squares as $s) {
			foreach ($this->unitlist as $u) {
				if (in_array($s, $u)) {
					$units[$s][] = $u;
					$peers[$s] = isset($peers[$s]) ? array_merge($peers[$s], array_flip($u)) : array_flip($u);
				}
			}
			unset($peers[$s][$s]);
			$peers[$s] = array_keys($peers[$s]);
		}
		$this->units = $units;
		$this->peers = $peers;

	}

	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}

	public function log($str) {
		file_put_contents('log.txt', "$str\n", FILE_APPEND);
	}

	private function cross($arr1, $arr2) {
		
		foreach ($arr1 as $a) {
			foreach ($arr2 as $b) {
				$result[] = $a . $b;
			}
		}

		return $result;

	}

	public function unconstrained_grid() {
		foreach ($this->squares as $s) {
			$values[$s] = $this->digits;
		} 
		return $values;
	}

	public function parse_grid($grid) {
		
		$values = $this->unconstrained_grid();
		
		foreach ($this->grid_values($grid) as $square => $char) {
			
				assert(is_string($char));

				if (in_array($char, $this->digits) && ! $this->assign($values, $square, $char)) {
					return false;
				}		
		}
		
		return $values;
	}

	public function grid_values($grid) {
		$grid = str_split($grid);
		foreach ($grid as $char) {
			if ( in_array( $char, array_merge($this->digits, ['0','.']))) {
				$chars[] = $char;
			}
		}
		assert(count($chars) === 81);
		return array_combine($this->squares, $chars);
	}

	public function get_other_values($square, $char) {

		return array_diff($square, [$char]);

	}

	public function assign(&$values, $square, $char) {

		$this->log("Assigning $char to $square:");
		
		$other_values = $this->get_other_values($values[$square], $char);
		
		foreach ($other_values as $other_char) {

			assert(is_string($other_char));

			if ( ! $this->eliminate($values, $square, $other_char) ) {
				return false;
			}
		}

		$this->log("$square now contains " . serialize($values[$square]));

		return $values;
	}

	public function eliminate(&$values, $square, $char) {

		assert(!is_array($char));

		$this->log("Eliminating $char from $square:");

		if ( ! in_array($char, $values[$square]) ) {
			$this->log("$char already eliminated from $square. No action.");
			return $values;
		}
		$values[$square] = $this->get_other_values($values[$square], $char);
		if (count($values[$square]) === 0) {
			return false;
		}
		else if (count($values[$square]) === 1) {
			$other_chars = $values[$square];
			foreach ($this->peers[$square] as $other_square) {
				foreach ($other_chars as $other_char) {
					assert(is_string($other_char));
					if ( ! $this->eliminate($values, $other_square, $other_char)) {
						return false;
					}
				}
			}
		}
		foreach ($this->units[$square] as $unit) {
			$charplaces = [];
			foreach ($unit as $sq) {
				if (in_array($char, $values[$sq])) {
					$charplaces[] = $sq;
				}
			}
			if (count($charplaces) === 0) { return false; }
			else if (count($charplaces) === 1) {
				if ( ! $this->assign($values, $charplaces[0], $char) ) {
					return false;
				}
			}
		}
		
		$this->log("$square now contains " . serialize($values[$square]));

		return $values;
	}

	public function display($values) {
		$maxlen = 0;
		foreach ($this->squares as $s) {
			if (count($values[$s] > $maxlen)) { $maxlen = count($values[$s]); }
		}
		$width = $maxlen + 2;
		$line = implode('+', array_fill(0, 3, str_repeat('-', $width * 3)));
		foreach ($this->rows as $r) {
			foreach ($this->cols as $c) {
				$item = implode($values[$r.$c]);
				echo str_pad($item, $width, " ", STR_PAD_BOTH);
				if ($c === '3' || $c === '6') { echo '|'; }
			}
			if ($r === 'C' || $r === 'F') { echo "\n$line"; };
			echo "\n";
		}	
	}

	public function solution() {
		return $this->display($this->search($this->parse_grid($this->grid)));
	}

	public function search($values) {
		if (!$values) {
			return false;
		}
		$solved = true;
		foreach ($this->squares as $square) {
			if (count($values[$square]) !== 1) {
				$solved = false;
			}
		}
		if ($solved) { return $values; }
		$min = 9;
		foreach ($this->squares as $square) {
			if (count($values[$square]) > 1 && count($values[$square]) <= $min) {
				$min = count($values[$square]);
				$candidate = $square;
			}
		}
		foreach ($values[$candidate] as $char) {
			$values_copy = $values;
			$assign_attempt = $this->assign($values_copy, $candidate, $char);
			$try = $this->search($assign_attempt);
			if ($try) { return $try; }
		}
		return false;
	}

}