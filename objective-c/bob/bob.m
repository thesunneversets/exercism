//
//  Bob.m
//  bob
//
//  Created by Matthew Marcus on 11/15/13.
//  Copyright (c) 2013 Matthew Marcus. All rights reserved.
//

#import "Bob.h"

@implementation Bob

-(NSString *)hey:(NSString *)input {
    
    Boolean silence =  [input isEqualToString:@""];
    Boolean question = [input hasSuffix:@"?"];
    Boolean shouting = [input isEqualToString:[input uppercaseString]];
    
    if (silence) {
        return @"Fine, be that way.";
    } else if (question) {
        return @"Sure.";
    } else if (shouting) {
        return @"Woah, chill out!";
    } else {
        return @"Whatever.";
    }
}

@end