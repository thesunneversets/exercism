defmodule DNA do

    def hamming_distance(strand1, strand2) do
      List.zip([strand1, strand2]) |> Enum.count &Kernel.!=/2
    end

end