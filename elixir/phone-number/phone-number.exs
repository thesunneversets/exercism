defmodule Phone do

  defp clean(str) do
	str |> String.replace(%r/\D/, "")
  end

  defp validate(str) do
    cond do
      String.length(str) == 11 and String.starts_with?(str, "1") ->
        String.slice(str, 1, 10)
      String.length(str) == 10 ->
        str
      true ->
        "0000000000"
    end
  end

  def number(str) do
	str |> clean |> validate
  end

  def area_code(str) do
    str |> number |> String.slice(0,3)
  end

  def pretty(str) do
	str |> number |> String.replace %r\(.{3})(.{3})(.{4})\, %S"(\1) \2-\3"
  end

end