defmodule Words do 

	defp strip_punctuation(phrase) do
	  Regex.replace(%r/[^\w ]/, phrase, "")
	end

	defp update_count(word, hash) do
	  Dict.update(hash, word, 1, &1 + 1)
	end

	def count(phrase) do
	  phrase |> String.downcase 
	         |> strip_punctuation
	         |> String.split
	         |> Enum.reduce(HashDict.new, &update_count/2)
	end

end