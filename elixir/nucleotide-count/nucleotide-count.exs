defmodule DNA do

  def count(dna, nucleotide) do
    dna |> to_char_list 
        |> Enum.count (&1 == nucleotide)
  end

  def nucleotide_counts(dna) do
    HashDict.new [?A, ?C, ?G, ?T], &{&1, count(dna, &1)}
  end

end