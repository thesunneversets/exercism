defmodule School do 

  def add(db, pupil, grade) do
    HashDict.put(db, grade, grade(db, grade) ++ [pupil])
  end

  def grade(db, grade) do
  	HashDict.get(db, grade, [])
  end

  def sort(db) do
    HashDict.new(Enum.map(db, fn {k, v} -> {k, Enum.sort(v)} end))
  end

end