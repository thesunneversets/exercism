defmodule DNA do

 def to_rna(dna) do
   Enum.map dna, &(if &1 === ?T do ?U else &1 end)
 end

end