class Words

  tokenize: (phrase) ->
    phrase.match /\w+/g

  normalize: (phrase) ->
    phrase.toLowerCase()

  countWord: (freqs, word) ->
    freqs[word] ?= 0
    freqs[word]++
    freqs

  constructor: (phrase) ->
    words = @tokenize @normalize phrase
    @count = words.reduce @countWord, {} 

module.exports = Words