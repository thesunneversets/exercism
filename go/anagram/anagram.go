// Package anagram selects valid correct anagrams of a given word
// from a list of candidates.
package anagram

import (
    "sort"
    "strings"
    "reflect"
)

// Detect takes a word and a list of candidates and returns a sublist
// of the candidates that contain all the same letters in a different
// order. (A word is not an "anagram" of itself.)
func Detect(subject string, candidates []string) (result []string) {
    result = []string{}
    subject = strings.ToLower(subject)
    for _, candidate := range candidates {
        candidate = strings.ToLower(candidate)
        if reflect.DeepEqual(normalize(subject), normalize(candidate)) &&
            subject != candidate {
            result = append(result, candidate)
        }
    }
    return
}

func normalize(subject string) []string {
    runes := strings.Split(subject, "")
    sort.Strings(runes)
    return runes
}