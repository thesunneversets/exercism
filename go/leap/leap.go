package leap

func IsLeapYear(year int) bool {
	return isDivisibleBy(year, 400) ||
		(isDivisibleBy(year, 4) && !isDivisibleBy(year, 100))
}

func isDivisibleBy(year, divisor int) bool {
	return year%divisor == 0
}