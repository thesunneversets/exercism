package raindrops

import (
	"strconv"
)

func Convert(i int) (result string) {
	result = ""
	if i%3 == 0 {
		result += "Pling"
	}
	if i%5 == 0 {
		result += "Plang"
	}
	if i%7 == 0 {
		result += "Plong"
	}
	if result == "" {
		result = strconv.Itoa(i)
	}
	return
}