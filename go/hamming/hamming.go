package hamming

func Distance(strand1, strand2 string) (result int) {
	if len(strand2) < len(strand1) {
		strand1, strand2 = strand2, strand1
	}
	for i, c := range strand1 {
		if c != rune(strand2[i]) {
			result++
		}
	}
	return
}